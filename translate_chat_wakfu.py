#! /usr/bin/env python
"""
MIT License

Copyright (c) 2023 Nestero

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import time
import os
from pathlib import Path
from pylingva import pylingva
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

class Handler(FileSystemEventHandler):
    def on_modified(self, event):
        with open("translate.txt", "r") as translate:
            x = translate.read()
        os.system("clear")
        print(x)

def lastLine(sourceFile):
    with open(sourceFile, 'r') as f:
        if os.stat(sourceFile).st_size == 0:
            last = "-"
        else:
            last = f.readlines()[-1]
    return last

def colorChat(chat, txt):
    if chat == "Trade":
        text = f"[1;33m {txt}"
    elif chat == "Recruitment":
        text = f"[1;31m {txt}"
    elif chat == "Group":
        text = f"[1;34m {txt}"
    elif chat == "Guild":
        text = f"[1;32m {txt}"
    else:
        text = f"[1;37m {txt}"
    return text

def searchChat(txt):
    listChat = ["Trade", "Recruitment", "Group", "Guild", "Private"]
    text = ""
    for c in listChat:
        if c in txt:
            text = colorChat(c, txt)
            break
        else:
            text = f"[1;37m {txt}"
    return text

trans = pylingva()
event_handler = Handler()
observer = Observer()
observer.schedule(event_handler, path='.', recursive=False)
with open("translate.txt", 'x') as fileTranslate:
        pass
observer.start()

try:
    while True:
        time.sleep(1)
        with open("sort.txt", "a") as sort:
            lastSource = lastLine("wakfu_chat.log")
            lastSource = lastSource.replace("/", "(slash)")
            lastSort = lastLine("sort.txt")
            textChat = searchChat(lastSource)
            Translate = trans.translate("auto", "id", textChat)
            if lastSource != lastSort  and "Game Log" not in lastSource and "Error message" not in lastSource:
                x = sort.write(lastSource)
                with open("translate.txt", "a") as translate:
                    z = translate.write(f"\033{Translate} \033[0;0m \n")
except KeyboardInterrupt:
    observer.stop()
    os.remove("translate.txt")
    os.remove("sort.txt")
observer.join()

